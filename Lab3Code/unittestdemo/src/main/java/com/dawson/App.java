package com.dawson;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
    }

    public int echo (int x) {
        return x;
    }

    // Method with bug that will onlny return x
    public int oneMore(int x) {
        return x; 
    }
}

package com.dawson;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }

    @Test
    public void testEcho() {
        App app = new App();
        int result = app.echo(5);

        assertEquals("echo(5) should return 5", 5, result);
    }

    @Test
    public void testOneMore() {
        // Arrange
        App app = new App();
        int wrongResult = app.oneMore(5);

        //Expeccted the result to be 6
        assertEquals("oneMore(5) should return 6", 6, wrongResult); 
    }
    
}
